﻿using System.Collections.Generic;
using System.Linq;

namespace StringOperations
{
    public class Reverter
    {
        public string Revert(string text)
        {
            List<string> list = text.Split(' ').ToList();
           
            string result = string.Empty;

            for (int i = list.Count-1; i >= 0; i--)
            {
                result += list[i] + " ";
            }

            return result.Trim();
        }
        public char Revolver(string text, int index)
        {
            int i = (index % text.Length) - 1;
            if (i == -1)
                i = text.Length-1;
            return text.ToArray()[i];
        }
    }
}
