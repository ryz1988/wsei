﻿using System.Linq;

namespace StringOperations
{
    public class DigitalCouter
    {
        public long Count(string number)
        {
            long result = 0;
            foreach (char a in number.ToArray()) 
            {
                if (int.TryParse(a.ToString(), out int digit))
                {
                    result += digit;
                }
            }
            return result;
        }
    }
}
