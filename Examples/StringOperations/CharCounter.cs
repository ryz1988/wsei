﻿using System.Collections.Generic;

namespace StringOperations
{
    public class CharCounter
    {
        private Dictionary<char, int> countedChars;

        public CharCounter() { }

        public CharCounter(string text) 
        {
            countedChars = CountChar(text);
        }

        public void UpdateString(string text) 
        {
            countedChars = CountChar(text);
        }

        public Dictionary<char, int> CountChar(string text)
        {
            var result = new Dictionary<char, int>();
            foreach(char a in text) 
            {
                if(result.ContainsKey(a))
                {
                    result[a]++;
                } else
                {
                    result[a] = 1;
                }
            }
            return result;
        }

        public int GetValue(char a) {
            if(countedChars.ContainsKey(a))
                return countedChars[a];
            return 0;
        }
    }
}
