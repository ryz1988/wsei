﻿namespace Currency
{
    public interface IRateProvider
    {
        double Zloty2EuroRate();
        double Euro2ZlotyRate();
    }
}
