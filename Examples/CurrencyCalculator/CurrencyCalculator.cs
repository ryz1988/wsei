﻿namespace Currency
{
    public class CurrencyCalculator
    {
        private IRateProvider _rateProvider;

        public CurrencyCalculator(IRateProvider rateProvider) 
        {
            _rateProvider = rateProvider;
        }

        public double ChangeZloty2Euro(double zloty) 
        {
            var rate = _rateProvider.Zloty2EuroRate();
            return zloty * rate;
        }

        public double ChangeEuro2Zloty(double euro)
        {
            return euro * _rateProvider.Euro2ZlotyRate();
        }
    }
}
