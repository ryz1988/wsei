﻿namespace Currency
{
    internal class RateProvider : IRateProvider
    {
        public double Euro2ZlotyRate()
        {
            return 4.25;
        }

        public double Zloty2EuroRate()
        {
            return 0.25;
        }
    }
}