﻿using Currency;
using System;

namespace Examples
{
    class Program
    {
        static void Main(string[] args)
        {
            var rateProvider = new RateProvider();

            var calculator = new CurrencyCalculator(rateProvider);

            Console.WriteLine("Example of usage: " + calculator.ChangeZloty2Euro(100));
        }
    }
}
