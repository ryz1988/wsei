using Moq;
using NUnit.Framework;

namespace Currency.Tests
{
    public class Tests
    {
        Mock<IRateProvider> _rateProviderMock;

        [SetUp]
        public void Setup()
        {
            _rateProviderMock = new Mock<IRateProvider>();
        }

        [TestCase(100, 25)]
        [TestCase(10, 2.5)]
        public void Zloty2Euro_ReturnCountedCurrency(double zloty, double euro)
        {
            _rateProviderMock.Setup(p => p.Zloty2EuroRate()).Returns(0.25);

            var calculator = new CurrencyCalculator(_rateProviderMock.Object);
            var result = calculator.ChangeZloty2Euro(zloty);
            
            Assert.AreEqual(euro, result);
            _rateProviderMock.Verify(p => p.Zloty2EuroRate(), Times.Once);
        }

        [TestCase(100, 25, 0.25)]
        [TestCase(10, 2.4, 0.24)]
        public void Zloty2EuroWithDifferentRate_ReturnCountedCurrency(double zloty, double euro, double rate)
        {
            _rateProviderMock.Setup(p => p.Zloty2EuroRate()).Returns(rate);

            var calculator = new CurrencyCalculator(_rateProviderMock.Object);
            var result = calculator.ChangeZloty2Euro(zloty);

            Assert.AreEqual(euro, result);
            _rateProviderMock.Verify(p => p.Zloty2EuroRate(), Times.Once);
        }

    }
}