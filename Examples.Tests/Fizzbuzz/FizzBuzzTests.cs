﻿using NUnit.Framework;

namespace FizzBuzz.Tests
{
    [TestFixture]
    public class FizzBuzzTests
    {
        [Test]
        public void GetOutput_InputIsDivisibleBy3And5_ReturnFizzBuzz()
        {
            var result = FizzBuzz.GetOutput(15);
            
            Assert.That(result, Is.EqualTo("FizzBuzz"));
        }
        
        [Test]
        public void GetOutput_InputIsDivisibleBy3Only_ReturnFizz()
        {
            var result = FizzBuzz.GetOutput(3);
            
            Assert.That(result, Is.EqualTo("Fizz"));
        }
        
        [Test]
        public void GetOutput_InputIsDivisibleBy5Only_ReturnBuzz()
        {
            var result = FizzBuzz.GetOutput(5);
            
            Assert.That(result, Is.EqualTo("Buzz"));
        }
        
        [TestCase(1, "1")]
        [TestCase(2, "2")]
        [TestCase(7, "7")]
        [TestCase(3, "Fizz")]
        public void GetOutput_InputIsNotDivisibleBy3Or5_ReturnTheSameNumber(int number, string res)
        {
            var result = FizzBuzz.GetOutput(number);
            
            Assert.That(result, Is.EqualTo(res));
        }

        [Test]
        public void GetOutput_OtherNumber_ReturnTheSameNumber()
        {
            Assert.That(() => FizzBuzz.GetOutput(-1), Throws.ArgumentException);
        }

    }
}